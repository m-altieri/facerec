package app;

import org.openimaj.image.Image;

/**
 * Elaboratore di immagini che prende in ingresso un'immagine di tipo T, e restituisce un'immagine
 * di tipo U.
 * Oltre al tipo può effettuare altre modifiche, come dimensione, colore, ecc.
 * @author Massimiliano Altieri
 *
 */
public interface PhotoProcessor<T extends Image<?, ?>, U extends Image<?, ?>> {

	/**
	 * Elabora l'immagine e ne restituisce una nuova.
	 */
	public U process(T image);
}
