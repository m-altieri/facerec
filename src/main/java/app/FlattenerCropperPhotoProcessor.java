package app;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;
import org.openimaj.image.processing.resize.ResizeProcessor;

import utils.Settings;

/**
 * PhotoProcessor che ritaglia la foto per adattarle tutte alla stessa dimensione, e poi le trasforma in bianco e nero.
 * @author Massimiliano Altieri
 */
public class FlattenerCropperPhotoProcessor implements PhotoProcessor<MBFImage, FImage> {

	private int width;
	private int height;
	
	/**
	 * Crea un nuovo FlattenerCropperPhotoProcessor con le dimensioni di ritaglio passate come argomento.
	 * @param w La larghezza del ritaglio.
	 * @param h L'altezza del ritaglio.
	 */
	public FlattenerCropperPhotoProcessor(int w, int h) {
		width = Settings.IMAGES_WIDTH;
		height = Settings.IMAGES_HEIGHT;
	}
	
	/**
	 * Crea un nuovo FlattenerCropperPhotoProcessor con le dimensioni di ritaglio definite nel file Settings.
	 */
	public FlattenerCropperPhotoProcessor() {
		this(Settings.IMAGES_WIDTH, Settings.IMAGES_HEIGHT);
	}
	
	@Override
	public FImage process(MBFImage image) {
		
		FImage greyFrame = image.flatten();
		
		int cropSize = greyFrame.width - this.width * greyFrame.height / this.height;
		int cropLeft = cropSize / 2;
		int cropRight = greyFrame.width - cropLeft;
		for (int i = 0; i < greyFrame.width; i++) {
			for (int j = 0; j < greyFrame.height; j++) {
				if (i < cropLeft || i > cropRight) {
					greyFrame.setPixel(i, j, 0f);
				}
			}
		}
		greyFrame = greyFrame.trim();
		ResizeProcessor processor = new ResizeProcessor(this.width, this.height, false);
		processor.processImage(greyFrame);
		
		return greyFrame;
	}
}
