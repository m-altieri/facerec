package app;

import org.openimaj.image.MBFImage;
import org.openimaj.video.Video;
import org.openimaj.video.VideoDisplay;
import org.openimaj.video.capture.VideoCapture;
import org.openimaj.video.capture.VideoCaptureException;

import utils.Settings;

/**
 * Implementazione di VideoSource per sorgenti video a colori.
 * Permette di acquisire fotogrammi a colore dalla webcam.
 * @author Massimiliano Altieri
 */
public class MBFImageVideoSource implements VideoSource<MBFImage> {

	private Video<MBFImage> video;
	private VideoDisplay<MBFImage> display;
	
	private final static int DEFAULT_VIDEOWIDTH = Settings.IMAGES_WIDTH;
	private final static int DEFAULT_VIDEOHEIGHT = Settings.IMAGES_HEIGHT;
	
	private int width;
	private int height;
	
	/**
	 * Crea una nuova VideoSource a colori con larghezza e altezza a scelta.
	 * @param w Larghezza del video.
	 * @param h Altezza del video.
	 * @throws VideoCaptureException Se c'è un problema con l'accesso al dispositivo di ripresa.
	 */
	public MBFImageVideoSource(int w, int h) throws VideoCaptureException {
		width = w;
		height = h;
		video = new VideoCapture(width, height);
		display = VideoDisplay.createVideoDisplay(video);
		display.getScreen().getTopLevelAncestor().setVisible(false);
	}
	
	/**
	 * Crea una nuova VideoSource a colori con larghezza e altezza definite nel file Settings.
	 * @throws VideoCaptureException Se c'è un problema con l'accesso al dispositivo di ripresa.
	 */
	public MBFImageVideoSource() throws VideoCaptureException {
		this(DEFAULT_VIDEOWIDTH, DEFAULT_VIDEOHEIGHT);
	}
	
	@Override
	public MBFImage getFrame() {
		
		return video.getCurrentFrame();
	}

	@Override
	public void open() {
		try {
			video = new VideoCapture(width, height);
		} catch (VideoCaptureException e) {
			System.out.println("Impossibile aprire il video");
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		video.close();
	}
	
	/**
	 * Resituisce la larghezza della sorgente video.
	 * @return La larghezza.
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Resituisce l'altezza della sorgente video.
	 * @return L'altezza.
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Resituisce l'intera finestra nella quale la sorgente video viene riprodotta.
	 * @return L'intera finestra.
	 */
	public VideoDisplay<MBFImage> getDisplay() {
		return display;
	}
	
}
