package app;

import org.openimaj.image.Image;

/**
 * Sorgente video dalla quale si possono ottenere fotogrammi.
 * @author Massimiliano Altieri
 */
public interface VideoSource<T extends Image<?, ?>> {

	/**
	 * Ottieni il fotogramma attualmente attivo nella sorgente video.
	 * @return Il fotogramma attualmente attivo.
	 */
	public T getFrame();
	
	/**
	 * Apri la sorgente video.
	 */
	public void open();
	
	/**
	 * Chiudi la sorgente video.
	 */
	public void close();
}
