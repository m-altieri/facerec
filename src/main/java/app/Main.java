package app;

import gui.ToolsWindow;

/**
 * Classe principale dell'applicazione.
 * @author Massimiliano Altieri
 */
public class Main {

	/**
	 * Metodo principale dell'applicazione.
	 */
	public static void main(String[] args) {

		ToolsWindow toolsWindow = new ToolsWindow();
		toolsWindow.show();
	}
}
