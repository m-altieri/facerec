package engine;

import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import org.apache.commons.vfs2.FileSystemException;
import org.openimaj.data.dataset.GroupedDataset;
import org.openimaj.data.dataset.ListDataset;
import org.openimaj.data.dataset.VFSGroupDataset;
import org.openimaj.experiment.dataset.split.GroupedRandomSplitter;
import org.openimaj.experiment.dataset.util.DatasetAdaptors;
import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.model.EigenImages;

import exceptions.UncertainIdentityException;
import utils.Pair;
import utils.Settings;

/**
 * Motore principale dell'algoritmo di recommendation.
 * Fornisce tutte le funzionalità necessarie alla GUI.
 * Le sue componenti principali sono due SwingWorker: uno che esegue le operazioni di
 * identificatione, e l'altro quelle di verifica.
 * @author Massimiliano Altieri
 */
public class EigenfacesEngine {
	
	private static final float UNKNOWN_THRESHOLD = Settings.UNKNOWN_THRESHOLD;
	private static final float UNCERTAIN_THRESHOLD = Settings.UNCERTAIN_THRESHOLD;
	
	private static final int N_EIGENVECTORS = Settings.N_EIGENVECTORS;
	static final String PATH = "C:/Users/PC/git/facerec/photos"; 
	
	private static final int TRAINING_SIZE = 5;
	private static final int TEST_SIZE = 5;
	
	
	private List<SwingWorker<?, ?>> workers;
	private SwingWorker<Pair<String, Double>, Pair<String, Double>> identificationWorker;
	private SwingWorker<Double, Double> verificationWorker;
	
	/**
	 * Crea un EigenfacesEngine con le impostazioni predefinite.
	 */
	public EigenfacesEngine() {
		
		identificationWorker = new IdentificationWorker();
		verificationWorker = new VerificationWorker();
		
		workers = new ArrayList<SwingWorker<?, ?>>();
		workers.add(identificationWorker);
		workers.add(verificationWorker);
	}
	
	/**
	 * Imposta un listener per la modifica delle proprietà dello SpringWorker relativo all'identificazione.
	 * Il listener riceverà una notifica quando il lavoro dello SpringWorker è terminato.
	 * @param listener Il listener.
	 */
	public void setIdentificationPropertyChangeListener(PropertyChangeListener listener) {
		identificationWorker.addPropertyChangeListener(listener);
	}
	
	/**
	 * Imposta un listener per l'elaborazione dei chunks intermedi emessi dallo SpringWorker
	 * relativo all'identificazione durante il suo processo.
	 * @param Il listener.
	 */
	public void setIdentificationChunksListener(ChunksListener<Pair<String, Double>> listener) {
		((IdentificationWorker) identificationWorker).setChunksListener(listener);
	}
	
	/**
	 * Imposta un listener per la modifica delle proprietà dello SpringWorker relativo alla verifica.
	 * Il listener riceverà una notifica quando il lavoro dello SpringWorker è terminato.
	 * @param Il listener.
	 */
	public void setVerificationPropertyChangeListener(PropertyChangeListener listener) {
		verificationWorker.addPropertyChangeListener(listener);
	}
	
	
	/**
	 * Imposta un listener per l'elaborazione dei chunks intermedi emessi dallo SpringWorker
	 * relativo alla verifica durante il suo processo.
	 * @param Il listener.
	 */
	public void setVerificationChunksListener(ChunksListener<Double> listener) {
		((VerificationWorker) verificationWorker).setChunksListener(listener);
	}
	
	/**
	 * Resituisce il risultato del lavoro dello SpringWorker relativo all'identificazione.
	 * Se quando si chiama questo metodo lo SpringWorker non ha ancora finito, il processo attuale attenderà
	 * fino al suo completamento; pertanto, è consigliabile chiamare questo metodo in seguito a una callback
	 * di cambio di stato che notifica della fine del processo.
	 * @return Il risultato dello SpringWorker, ovvero una coppia di valori formata dal <i>nome</i> del soggetto
	 * identificato, e dal <i>livello di incertezza</i> dell'identificazione.
	 * Un valore alto significa più incertezza.
	 * Nel caso in cui non sia stato possibile identificare nessun individuo, ovvero se il livello di incertezza
	 * supera una certa soglia, questo metodo restituisce <i>null</i>.
	 * @throws UncertainIdentityException Se il livello di incertezza supera una soglia di threshold.
	 * E' comunque possibile utilizzare i valori ottenuti ricavandoli direttamente dall'eccezione, tramite i metodi
	 * getName() e getUncertainty().
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public Pair<String, Double> getIdentificationWorkerResult() throws UncertainIdentityException, InterruptedException, ExecutionException {
		Pair<String, Double> result = identificationWorker.get();
		
		System.out.println("[EigenfacesEngine] Distance: " + result.value());
		System.out.println("[EigenfacesEngine] Unknown Identity Threshold: " + UNKNOWN_THRESHOLD);
		System.out.println("[EigenfacesEngine] Uncertain Identity Threshold: " + UNCERTAIN_THRESHOLD);
		
		Pair<String, Double> ret = null;
		
		if (result.value() > UNKNOWN_THRESHOLD) {
			ret = null;
		} else if (result.value() < UNKNOWN_THRESHOLD && result.value() > UNCERTAIN_THRESHOLD) {
			throw new UncertainIdentityException(result.key(), result.value());
		} else {
			ret = result;
		}
		
		return ret;
	}
	
	/**
	 * Resituisce il risultato del lavoro dello SpringWorker relativo alla verifica.
	 * Se quando si chiama questo metodo lo SpringWorker non ha ancora finito, il processo attuale attenderà
	 * fino al suo completamento; pertanto, è consigliabile chiamare questo metodo in seguito a una callback
	 * di cambio di stato che notifica della fine del processo.
	 * @return Il risultato dello SpringWorker, ovvero il <i>livello di incertezza</i> della verifica.
	 * Un valore alto significa più incertezza.
	 * Nel caso in cui il livello di incertezza supera una certa soglia, questo metodo restituisce <i>null</i>.
	 * @throws UncertainIdentityException Se il livello di incertezza supera una soglia di threshold.
	 * E' comunque possibile utilizzare i valori ottenuti ricavandoli direttamente dall'eccezione, tramite il metodo
	 * getUncertainty().
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	public Double getVerificationWorkerResult() throws UncertainIdentityException, InterruptedException, ExecutionException {
		Double result = verificationWorker.get();
		
		System.out.println("[EigenfacesEngine] Distance: " + result);
		System.out.println("[EigenfacesEngine] Unknown Identity Threshold: " + UNKNOWN_THRESHOLD);
		System.out.println("[EigenfacesEngine] Uncertain Identity Threshold: " + UNCERTAIN_THRESHOLD);
		
		Double ret = null;
		
		if (result > UNKNOWN_THRESHOLD) {
			ret = null;
		} else if (result < UNKNOWN_THRESHOLD && result > UNCERTAIN_THRESHOLD) {
			throw new UncertainIdentityException(result);
		} else {
			ret = result;
		}
		
		return ret;
	}

	@SuppressWarnings("unused")
	private static void debugDistances() throws FileSystemException {

		// Carica il dataset
		VFSGroupDataset<FImage> dataset = new VFSGroupDataset<FImage>(PATH, ImageUtilities.FIMAGE_READER);	
		
		// Crea training set e test set dividendo casualmente le facce relative a ogni persona
		GroupedRandomSplitter<String, FImage> splitter = 
				new GroupedRandomSplitter<String, FImage>(dataset, TRAINING_SIZE, 0, TEST_SIZE);
		GroupedDataset<String, ListDataset<FImage>, FImage> trainingSet = splitter.getTrainingDataset();
		GroupedDataset<String, ListDataset<FImage>, FImage> testSet = splitter.getTestDataset();
		
		// Mette il training set in una lista
		List<FImage> basisImages = DatasetAdaptors.asList(trainingSet);
		
		// nEigenvectors è il numero di vettori da usare come base per lo spazio delle facce
		// numeri comuni sono 100-150, che consentono una buona approssimazione di qualunque faccia umana
		// il massimo è dato dal numero totale di facce nel training set, ad esempio in questo caso
		// è 200 perchè sono 40 gruppi di facce ognuno con 5 facce di training
		final int nEigenvectors = N_EIGENVECTORS; // per usarle tutte fare = nTraining * trainingSet.getGroups().size();
		
		// creazione del motore di training, ovvero il responsabile della creazione della base (eigenfaces)
		// crea gli eigenfaces in base a quanti ne ho indicati prima
		EigenImages eigen = new EigenImages(nEigenvectors);
		eigen.train(basisImages);
				
		// Mostra gli eigenfaces, più che altro per debug
		List<FImage> eigenfaces = new ArrayList<FImage>();
		for (int i = 0; i < eigen.getNumComponents(); i++) { // i < 12
			eigenfaces.add(eigen.visualisePC(i));
//			DisplayUtilities.display(eigen.visualisePC(i));
//			System.out.println(i);
		}
//		DisplayUtilities.display("Eigenfaces", eigenfaces);
		
		// Crea il database delle feature di tutte le persone, da usare per il confronto di similarità
		// ovvero crea una mappa contenente per ogni persona, una serie di feature vector, ognuno corrispondente
		// a una sua immagine del training set, ognuno formato dalla combinazione lineare degli eigenfaces
		Map<String, DoubleFV[]> features = new HashMap<String, DoubleFV[]>();
		for (String person : trainingSet.getGroups()) {
			DoubleFV[] fvs = new DoubleFV[TRAINING_SIZE];
			
			for (int i = 0; i < fvs.length; i++) {
				FImage face = trainingSet.get(person).get(i);
				fvs[i] = eigen.extractFeature(face);
			}
			features.put(person, fvs);
		}
		
		// Confronta ogni immagine del test set e calcola l'accuratezza del sistema, in questo modo:
		// prende una persona alla volta dal test set, ne estrae le feature (la combinazione lineare
		// degli eigenfaces), e confronta queste feature con tutte le altre immagini del training set,
		// per stimare la sua vera identità in base alla persona la cui immagine si avvicina di più
		// in questo caso con la distanza euclidea
		double correct = 0;
		double incorrect = 0;
		double unknown = 0;
		for (String truePerson : testSet.getGroups()) {
			for (FImage face : testSet.get(truePerson)) {
				DoubleFV testFeature = eigen.extractFeature(face);
				
				String bestPerson = null;
				double minDistance = Double.MAX_VALUE;
				for (String person : features.keySet()) {
					for (DoubleFV fv : features.get(person)) {
						double distance = fv.compare(testFeature, DoubleFVComparison.EUCLIDEAN);
						
						if (distance < minDistance) {
							minDistance = distance;
							bestPerson = person;
						}
					}
				}
				
				System.out.print("Actual: " + truePerson + "\tGuess: " + bestPerson + "\tDistance: " + minDistance);
				
				if (minDistance > UNKNOWN_THRESHOLD) {
					unknown++;
					System.out.println("\t?");
				} else if (truePerson.equals(bestPerson)) {
					correct++;
					System.out.println();
				} else {
					incorrect++;
					System.out.println("\tX");
				}
			}
		}
		
		System.out.println("Accuracy: " + (correct / (correct + incorrect)));
		System.out.println("Unknown Rate: " + (unknown / (unknown + correct + incorrect)));
	}
	
	/**
	 * Prende in input un'immagine e calcola la persona più simile fra tutte quelle inserite nel sistema.
	 * Lo scopo del metodo è capire <b>chi è</b> la persona in input, e deve essere chiamato se
	 * tale persona <b>esiste</b>. In altre parole l'esistenza a priori della persona è un <i>prerequisito</i>,
	 * altrimenti il metodo può essere imprevedibile.
	 * Questo metodo non restituisce direttamente un risultato, ma quest'ultimo è recuperabile
	 * tramite getIdentificationWorkerResult().
	 * @param candidate Immagine del volto del soggetto da identificare, già processata per essere
	 * compatibile con le altre.
	 * @see getIdentificationWorkerResult()
	 */
	public void identify(FImage candidate) {
		((IdentificationWorker) identificationWorker).setImage(candidate);
		identificationWorker.execute();
	}
	
	/**
	 * Prende in input un'utente del sistema e un'immagine e calcola se le due identità corrispondono.
	 * Lo scopo del metodo è capire se il volto inserito appartiene davvero alla persona selezionata.
	 * Questo metodo non restituisce direttamente un risultato, ma quest'ultimo è recuperabile
	 * tramite getVerificationWorkerResult().
	 * @param Il nome del soggetto da verificare e l'immagine del volto già processata per essere
	 * compatibile con le altre.
	 * @see getVerificationWorkerResult()
	 */
	public void verify(String name, FImage photo) {
		((VerificationWorker) verificationWorker).set(name, photo);
		verificationWorker.execute();
	}

	/**
	 * Converte la distanza tra due volti in un valore che ne rappresenta la similarità, 
	 * che può essere vista come la probabilità che le due identità coincidano.
	 * La conversione avviene tramite una formula matematica ricavata empiricamente.
	 * Attualmente utilizzo la seguente formula:
	 * <i>f(x) = 104 - x^(3/2) * 0.9</i><br/>
	 * Il risultato massimo è 100.
	 * @param distance La distanza tra i due volti.
	 * @return Un indice di similarità.
	 */
	public double distanceToSimilarity(double distance) {
		double similarity = 0.0;
		
		similarity = 104 - Math.pow(distance, 3.0 / 2.0) * 0.9;
		
		similarity = Math.min(100, similarity);
		similarity = Math.max(0, similarity);
		
		return similarity;
	}

}
