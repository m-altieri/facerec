package engine;

import java.util.List;

/**
 * Riceve i chunks intermedi di un processo da uno SpringWorker.
 * @author Massimiliano Altieri
 *
 * @param <U> Il tipo dei chunks.
 */
public interface ChunksListener<U> {

	/**
	 * Esegue delle operazioni sui chunks ricevuti.
	 * @param chunks I chunks ricevuti.
	 */
	void chunksPublished(List<U> chunks);
	
}
