package engine;

import java.util.List;

import javax.swing.SwingWorker;

import org.openimaj.data.dataset.VFSGroupDataset;
import org.openimaj.data.dataset.VFSListDataset;
import org.openimaj.experiment.dataset.util.DatasetAdaptors;
import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.model.EigenImages;

import utils.Files;
import utils.Settings;

/**
 * SpringWorker con il compito di verificare se l'identità di un soggetto corrisponde con il nome fornito in input.
 * @author Massimiliano Altieri
 */
public class VerificationWorker extends SwingWorker<Double, Double> {
	
	private static final String PATH = EigenfacesEngine.PATH;

	private ChunksListener<Double> chunksListener = basicChunksListener();
	String name;
	FImage image;
	
	VerificationWorker() {
		super();
	}
	
	void set(String name, FImage image) {
		this.name = name;
		this.image = image;
	}
	
	private ChunksListener<Double> basicChunksListener() {
		return new ChunksListener<Double>() {

			@Override
			public void chunksPublished(List<Double> chunks) {
				System.out.println("Basic chunks listener does nothing!");
			}
			
		};
	}
	
	@Override
	protected Double doInBackground() throws Exception {
		
		EigenImages eigen = new EigenImages(Settings.N_EIGENVECTORS);
		VFSGroupDataset<FImage> dataset = new VFSGroupDataset<FImage>(PATH, ImageUtilities.FIMAGE_READER);	
		dataset.getGroups().retainAll(Files.getCompleteUsers());

		List<FImage> trainingImages = DatasetAdaptors.asList(dataset);

		eigen.train(trainingImages);
		
		DoubleFV[] claimedIdentityFeatures = new DoubleFV[Settings.N_PHOTOS];
		VFSListDataset<FImage> storedPhotos = dataset.get(name);
		
		for (int i = 0; i < claimedIdentityFeatures.length; i++) {
			FImage face = storedPhotos.get(i);
			claimedIdentityFeatures[i] = eigen.extractFeature(face);
		}

		DoubleFV testSubject = eigen.extractFeature(image);

		double minDistance = Double.MAX_VALUE;
		double meanDistance = 0.0;
		for (DoubleFV fv : claimedIdentityFeatures) {
			double distance = fv.compare(testSubject, DoubleFVComparison.EUCLIDEAN);
			
			if (distance < minDistance) {
				minDistance = distance;
			}
			meanDistance += distance;
		}
		meanDistance = meanDistance / claimedIdentityFeatures.length;
		
		System.out.println("MinDistance: " + minDistance);
		System.out.println("MeanDistance: " + meanDistance);
		
		return minDistance;
	}
	
	@Override
	protected void process(List<Double> chunks) {
		super.process(chunks);
		
		chunksListener.chunksPublished(chunks);
	}

	void setChunksListener(ChunksListener<Double> listener) {
		this.chunksListener = listener;
	}
	
}
