package engine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.SwingWorker;

import org.openimaj.data.dataset.VFSGroupDataset;
import org.openimaj.experiment.dataset.util.DatasetAdaptors;
import org.openimaj.feature.DoubleFV;
import org.openimaj.feature.DoubleFVComparison;
import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.model.EigenImages;

import utils.Files;
import utils.Pair;
import utils.Settings;

/**
 * SpringWorker con il compito di identificare l'identità di un soggetto all'interno del sistema.
 * @author Massimiliano Altieri
 */
public class IdentificationWorker extends SwingWorker<Pair<String, Double>, Pair<String, Double>> {
	
	private static final String PATH = EigenfacesEngine.PATH;
	
	private ChunksListener<Pair<String, Double>> chunksListener = basicChunksListener();
	FImage image;
	
	IdentificationWorker() {
		super();
	}
	
	private ChunksListener<Pair<String, Double>> basicChunksListener() {
		return new ChunksListener<Pair<String, Double>>() {

			@Override
			public void chunksPublished(List<Pair<String, Double>> chunks) {
				System.out.println("Basic chunks listener does nothing!");
			}
			
		};
	}
	
	void setImage(FImage image) {
		this.image = image;
	}
	
	void setChunksListener(ChunksListener<Pair<String, Double>> listener) {
		this.chunksListener = listener;
	}
	
	@Override
	protected Pair<String, Double> doInBackground() throws Exception {

		EigenImages eigen = new EigenImages(Settings.N_EIGENVECTORS);
		VFSGroupDataset<FImage> dataset = new VFSGroupDataset<FImage>(PATH, ImageUtilities.FIMAGE_READER);
		dataset.getGroups().retainAll(Files.getCompleteUsers());

		List<FImage> trainingImages = DatasetAdaptors.asList(dataset);

		eigen.train(trainingImages);
		
		Map<String, DoubleFV[]> features = new HashMap<String, DoubleFV[]>();
		for (String person : dataset.getGroups()) {
			DoubleFV[] fvs = new DoubleFV[Settings.N_PHOTOS];
			
			for (int i = 0; i < fvs.length; i++) {
				FImage face = dataset.get(person).get(i);
				fvs[i] = eigen.extractFeature(face);
			}
			features.put(person, fvs);
		}
		
		DoubleFV testImage = eigen.extractFeature(image);
		
		String bestPerson = null;
		double minDistance = Double.MAX_VALUE;
		for (String person : features.keySet()) {
			for (DoubleFV fv : features.get(person)) {
				double distance = fv.compare(testImage, DoubleFVComparison.EUCLIDEAN);
				
				if (distance < minDistance) {
					minDistance = distance;
					bestPerson = person;
				}
			}
		}
		
		return new Pair<String, Double>(bestPerson, minDistance);
	}
	
	@Override
	protected void process(List<Pair<String, Double>> chunks) {
		super.process(chunks);
		
		chunksListener.chunksPublished(chunks);
	}
	
}
