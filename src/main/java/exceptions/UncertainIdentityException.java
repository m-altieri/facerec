package exceptions;

/**
 * Eccezione lanciata quando viene trovata una identità plausibile per un certo soggetto, ma il sistema non può
 * dirsi abbastanza certo che sia quella esatta.
 * Delega al cliente la decisione sul da farsi nei casi in cui l'identità è in un range di incertezza.
 * @author Massimiliano Altieri
 */
@SuppressWarnings("serial")
public class UncertainIdentityException extends Exception {
	
	private String name;
	private double uncertainty;

	/**
	 * Crea una UncertainIdentityException con il livello di incertezza x.
	 * L'exception handler può usare questo valore tramite il metodo getUncertainty().
	 * @param x Il livello di incertezza.
	 */
	public UncertainIdentityException(double x) {
		uncertainty = x;
	}
	
	/**
	 * Crea una UncertainIdentityException con il nome s e il livello di incertezza x.
	 * L'exception handler può usare questi valori tramite i metodi getName() e getUncertainty().
	 * @param s Il nome del soggetto.
	 * @param x Il livello di incertezza.
	 */
	public UncertainIdentityException(String s, double x) {
		name = s;
		uncertainty = x;
	}
	
	/**
	 * Resituisce il nome del soggetto.
	 * @return Il nome.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Restituisce il livello di incertezza.
	 * @return Il livello di incertezza.
	 */
	public double getUncertainty() {
		return uncertainty;
	}
	
}
