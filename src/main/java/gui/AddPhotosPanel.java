package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.openimaj.video.capture.VideoCaptureException;

import gui.TakePhotoWindow.PhotoAddedListener;
import utils.Files;
import utils.Settings;

/**
 * Schermata per l'aggiunta di nuove foto per gli utenti del sistema.
 * @author Massimiliano Altieri
 */
@SuppressWarnings("serial")
public class AddPhotosPanel extends JPanel implements PhotoAddedListener {
	
	private JList<String> lstUsers;
	
	private JPanel photosPanel;
	private JList<String> lstPhotos;
	private JButton cmdAdd;
	private JButton cmdDel;
	
	private static final Font font = new Font("Arial", Font.PLAIN, 18);

	AddPhotosPanel() {
		
		super();
		
		setLayout(new GridLayout(1, 2));
		
		List<String> users = Files.getUsers();
		lstUsers = new JList<String>((String[]) users.toArray());
		lstUsers.setFont(font);
		lstUsers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstUsers.addListSelectionListener(new ListSelectionListener() {
			
			private String selection = "";
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!selection.equals(lstUsers.getSelectedValue())) {
					selection = lstUsers.getSelectedValue();
					cmdAdd.setEnabled(true);

					// change other listbox
					updatePhotosList();
				}
			}
			
		});
		
		photosPanel = new JPanel();
		photosPanel.setLayout(new FlowLayout());
		
		lstPhotos = new JList<String>(new String[] {});
		lstPhotos.setFont(font);
		lstPhotos.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				cmdDel.setEnabled(true);
			}
			
		});
		
		cmdAdd = new JButton();
		cmdAdd.setText("Aggiungi");
		cmdAdd.setFont(font);
		cmdAdd.setEnabled(false);
		cmdAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					TakePhotoWindow addPhotos = new TakePhotoWindow();
					addPhotos.setSubject(lstUsers.getSelectedValue());
					addPhotos.setVisible(true);
					addPhotos.setPhotoAddedListener(AddPhotosPanel.this);
					addPhotos.setTakePhotoListener(new PhotoSaverListener());
				} catch (VideoCaptureException f) {
					f.printStackTrace();
					System.out.println("Errore durante l'apertura della webcam");
				}
			}
			
		});
		
		cmdDel = new JButton();
		cmdDel.setText("Elimina");
		cmdDel.setFont(font);
		cmdDel.setForeground(Color.RED);
		cmdDel.setEnabled(false);
		cmdDel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// rimuovi foto
				File photoToDelete = new File(Settings.PHOTOS_PATH + lstUsers.getSelectedValue() + "\\" + lstPhotos.getSelectedValue());
				photoToDelete.delete();
				updatePhotosList();
				cmdDel.setEnabled(false);
			}
			
		});
				
		JScrollPane lstUsersScrollPane = new JScrollPane(lstUsers);
		
		JScrollPane lstPhotosScrollPane = new JScrollPane(lstPhotos);
		lstPhotosScrollPane.setPreferredSize(new Dimension(250, 150));
		
		photosPanel.add(lstPhotosScrollPane);
		photosPanel.add(cmdAdd);
		photosPanel.add(cmdDel);

		add(lstUsersScrollPane);
		add(photosPanel);
	}
	
	private void updatePhotosList() {
		List<String> photos = Files.getPhotos(lstUsers.getSelectedValue());
		lstPhotos.setListData((String[]) photos.toArray());
		paintAll(getGraphics());
	}

	@Override
	public void photoAdded() {
		this.updatePhotosList();
	}
}
