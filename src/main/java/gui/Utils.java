package gui;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Classe di utilità per la componente grafica.
 * @author Massimiliano Altieri
 */
public final class Utils {

	private static final String ICON_PATH = "res/icon.png";
	
	/**
	 * Restituisce l'icona con il logo.
	 * @return L'icona.
	 * @throws IOException
	 */
	public static Image getIcon() throws IOException {
		return ImageIO.read(new File(ICON_PATH));
	}
	
	private Utils() {
		// Cannot be instantiated.
	}
}
