package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingWorker.StateValue;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.video.capture.VideoCaptureException;

import engine.ChunksListener;
import engine.EigenfacesEngine;
import exceptions.UncertainIdentityException;
import utils.Files;
import utils.Settings;

/**
 * Schermata che fornisce il servizio di verifica.
 * Il servizio di verifica permette di inserire in input un nome e una foto, e di verificare se le due identità (nome e foto)
 * corrispondono, a patto che quel soggetto esista già tra gli utenti del sistema.
 * @author Massimiliano Altieri
 */
@SuppressWarnings("serial")
public class VerificationPanel extends JPanel implements TakePhotoListener<FImage>, PropertyChangeListener, ChunksListener<Double> {

	private JPanel leftPanel;
	private JPanel rightPanel;
	
	private FImage photo;
	private JPanel photoContainer;
	private JButton takePhoto;
	private JList<String> subject;
	private JButton confirm;
	
	private ImageIcon loadingIcon;
	private JLabel loading;
	
	private JTextField result;
	private JTextField confidence;
	
	private static final Font font = new Font("Arial", Font.PLAIN, 18);
	
	private EigenfacesEngine engine;
	
	VerificationPanel() {
		
		super();
		
		setLayout(new GridLayout(1, 2));
		
		leftPanel = new JPanel();
		rightPanel = new JPanel();
		photoContainer = new JPanel();
		takePhoto = new JButton();
		List<String> users = Files.getCompleteUsers();

		subject = new JList<String>((String[]) users.toArray());
		confirm = new JButton();
		result = new JTextField();
		confidence = new JTextField();
		engine = new EigenfacesEngine();
		
		loadingIcon = new ImageIcon(Settings.LOADING_ICON_PATH);
		loading = new JLabel("Loading...", loadingIcon, JLabel.CENTER);
		loading.setFont(font);
		
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		
		JPanel subjectPanel = new JPanel();
		subjectPanel.setLayout(new FlowLayout());
		subject.setFont(font);
		subject.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				takePhoto.setEnabled(true);
			}
			
		});
		
		JScrollPane subjectScrollPane = new JScrollPane(subject);
		subjectScrollPane.setPreferredSize(new Dimension(200, 200));		
		
		subjectPanel.add(subjectScrollPane);
		
		photoContainer.setPreferredSize(new Dimension(Settings.IMAGES_WIDTH, Settings.IMAGES_HEIGHT));
		
		takePhoto.setText("Scatta Foto");
		takePhoto.setFont(font);
		takePhoto.setPreferredSize(new Dimension(200, 40));
		takePhoto.setEnabled(false);
		takePhoto.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				TakePhotoWindow window;
				try {
					window = new TakePhotoWindow();
					window.setVisible(true);
					window.setMultiplePhotos(false);
					window.setTakePhotoListener(VerificationPanel.this);
					window.setSubject(subject.getSelectedValue());
				} catch (VideoCaptureException e1) {
					e1.printStackTrace();
				}
			}
			
		});
		
		confirm.setFont(font);
		confirm.setPreferredSize(new Dimension(250, 40));
		confirm.setEnabled(false);
		confirm.setText("VERIFICA");
		confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				takePhoto.setEnabled(false);
				subject.setEnabled(false);
				confirm.setEnabled(false);
				
				engine.setVerificationPropertyChangeListener(VerificationPanel.this);
				engine.setVerificationChunksListener(VerificationPanel.this);
				
				setLoading(true);
				
				engine.verify(subject.getSelectedValue(), photo);
			}
			
		});
		
		JLabel lblResult = new JLabel("Risultato");
		lblResult.setFont(font);
		JLabel lblUncertainty = new JLabel("Confidence");
		lblUncertainty.setFont(font);
		
		result.setFont(font);
		result.setPreferredSize(new Dimension(200, 40));
		result.setEnabled(false);
		confidence.setFont(font);
		confidence.setPreferredSize(new Dimension(70, 40));
		confidence.setEnabled(false);
		
		leftPanel.add(subjectPanel);
		leftPanel.add(photoContainer);
		leftPanel.add(takePhoto);
		
		rightPanel.add(confirm);
		rightPanel.add(lblResult);
		rightPanel.add(result);
		rightPanel.add(lblUncertainty);
		rightPanel.add(confidence);
		
		add(leftPanel);
		add(rightPanel);
	}
	
	private void setLoading(boolean set) {
		if (set) {
			rightPanel.add(loading);
		} else {
			rightPanel.remove(loading);
		}
		paintAll(getGraphics());
	}

	@Override
	public gui.TakePhotoListener.TakePhotoFlags photoTaken(String subject, FImage processedPhoto) {
		photo = processedPhoto;
		photoContainer.removeAll();
		photoContainer.add(new JLabel(new ImageIcon(ImageUtilities.createBufferedImage(photo))));
		confirm.setEnabled(true);
		paintAll(getGraphics());
		return TakePhotoFlags.SUCCESS;
	}

	@Override
	public void chunksPublished(List<Double> chunks) {
		System.out.println("Chunks received:");
		for (Double chunk : chunks) {
			System.out.println("Double: " + chunk.doubleValue());
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getNewValue() == StateValue.DONE) {
			Double engineResult = null;
			DecimalFormat decimalFormat = new DecimalFormat("##.##");
			try {
				engineResult = engine.getVerificationWorkerResult();
				if (engineResult != null) {
					result.setText("Accettato");
					confidence.setText(decimalFormat.format(engine.distanceToSimilarity(engineResult)));
				} else {
					result.setText("Rifiutato");
					confidence.setText("-");
				}
			} catch (UncertainIdentityException e) {
				System.out.println("Uncertain Identity");
				result.setText("Incerto");
				confidence.setText(String.valueOf(decimalFormat.format(engine.distanceToSimilarity(e.getUncertainty()))));
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} finally {
				setLoading(false);
			}
		}
	}

}
