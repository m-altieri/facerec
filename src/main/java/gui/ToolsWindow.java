package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import utils.Settings;

/**
 * Finestra principale del programma, dalla quale è possibile accedere a tutti i servizi.
 * @author Massimiliano Altieri
 */
public class ToolsWindow {

	private JFrame window;
	
	private final int WINDOW_WIDTH = Settings.WINDOW_WIDTH;
	private final int WINDOW_HEIGHT = Settings.WINDOW_HEIGHT;
	
	private JPanel mainPanel;
	
	/**
	 * Crea la finestra con le impostazioni predefinite.
	 */
	public ToolsWindow() {
		
		window = new JFrame();
		window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		window.setLayout(new BorderLayout());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setTitle("FaceRec");
		try {
			window.setIconImage(Utils.getIcon());
		} catch (IOException e) {
			System.out.println("Impossibile impostare l'icona");
			e.printStackTrace();
		}
		window.setResizable(false);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int locationX = screenSize.width / 2 - window.getWidth() / 2;
		int locationY = screenSize.height / 2 - window.getHeight() / 2;
		window.setLocation(locationX, locationY);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		JLabel mainPanelLabel = new JLabel("FaceRec", new ImageIcon("res/logo.png"), JLabel.CENTER);
		mainPanelLabel.setFont(new Font("Arial", Font.ITALIC, 48));
		mainPanel.add(mainPanelLabel, BorderLayout.CENTER);

		window.setJMenuBar(createMenuBar());
		
		window.add(mainPanel, BorderLayout.CENTER);
		
	}
	
	/**
	 * Restituisce la finestra.
	 * @return La finestra.
	 */
	public JFrame getWindow() {
		return window;
	}
	
	private JMenuBar createMenuBar() {
		
		JMenuBar menuBar = new JMenuBar();
		Font menuFont = new Font("Arial", Font.PLAIN, 18);
		Font menuItemFont = new Font("Arial", Font.PLAIN, 16);
		
		// Menu Utenti
		JMenu usersMenu = new JMenu();
		usersMenu.setText("Utenti");
		usersMenu.setFont(menuFont);
		
		JMenuItem addUsers = new JMenuItem("Aggiungi utenti");
		addUsers.setFont(menuItemFont);
		JMenuItem addPhotos = new JMenuItem("Aggiungi foto");
		addPhotos.setFont(menuItemFont);
		
		usersMenu.add(addUsers);
		usersMenu.add(addPhotos);
		
		addUsers.addActionListener(new AddUsersActionListener());
		addPhotos.addActionListener(new AddPhotosActionListener());
		
		// Menu Riconoscimento
		JMenu recognitionMenu = new JMenu();
		recognitionMenu.setText("Riconoscimento");
		recognitionMenu.setFont(menuFont);
		
		JMenuItem identification = new JMenuItem("Identificazione");
		JMenuItem verification = new JMenuItem("Verifica");
		identification.setFont(menuItemFont);
		verification.setFont(menuItemFont);
		
		recognitionMenu.add(identification);
		recognitionMenu.add(verification);
				
		identification.addActionListener(new IdentificationButtonListener());
		verification.addActionListener(new RecognitionButtonListener());
		
		// Barra Menu
		menuBar.add(usersMenu);
		menuBar.add(recognitionMenu);
		
		return menuBar;
	}
	
	private class AddUsersActionListener implements ActionListener {
		
		private JPanel panel;

		private JPanel createPanel() {
			return new AddUsersPanel();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			window.remove(mainPanel);
			panel = createPanel();
			mainPanel = panel;
			window.add(mainPanel);
			window.paintAll(window.getGraphics());
		}
	}
	
	private class AddPhotosActionListener implements ActionListener {

		private JPanel panel;
		
		private JPanel createPanel() {
			return new AddPhotosPanel();
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			panel = createPanel();
			changeMainPanel(panel);
		}
		
	}
	
	private class IdentificationButtonListener implements ActionListener {

		private JPanel panel;
		
		private JPanel createPanel() {
			return new IdentificationPanel();
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			panel = createPanel();
			changeMainPanel(panel);
		}
		
	}
	

	
	private class RecognitionButtonListener implements ActionListener {

		private JPanel panel;
		
		private JPanel createPanel() {
			return new VerificationPanel();
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			panel = createPanel();
			changeMainPanel(panel);
		}
		
	}
	
	private void changeMainPanel(JPanel newPanel) {
		window.remove(mainPanel);
		mainPanel = newPanel;
		window.add(mainPanel);
		window.paintAll(window.getGraphics());
	}
	
	/**
	 * Mostra la finestra.
	 */
	public void show() {
		
		window.setVisible(true);
	}
}
