package gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.openimaj.image.FImage;
import org.openimaj.image.MBFImage;
import org.openimaj.video.capture.VideoCaptureException;

import app.FlattenerCropperPhotoProcessor;
import app.MBFImageVideoSource;
import app.PhotoProcessor;
import app.VideoSource;
import gui.TakePhotoListener.TakePhotoFlags;

/**
 * Finestra che permette di scattare foto.
 * @author Massimiliano Altieri
 */
@SuppressWarnings("serial")
public class TakePhotoWindow extends JFrame implements WindowListener {
	
	interface PhotoAddedListener {
		
		void photoAdded();
	}

	private String name;
	private VideoSource<MBFImage> video;
	private JButton cmdTakePhoto;
	private JLabel result;
	
	private PhotoAddedListener photoAddedListener = basicPhotoAddedListener();
	
	private PhotoProcessor<MBFImage, FImage> photoProcessor;
	
	private static final Font font = new Font("Arial", Font.PLAIN, 18);
	
	private TakePhotoListener<FImage> takePhotoListener = basicTakePhotoListener();
	
	private boolean multiplePhotos;
	
	private PhotoAddedListener basicPhotoAddedListener() {
		return new PhotoAddedListener() {

			@Override
			public void photoAdded() {
				// Do nothing.
			}
			
		};
	}
	
	private TakePhotoListener<FImage> basicTakePhotoListener() {
		return new TakePhotoListener<FImage>() {

			@Override
			public gui.TakePhotoListener.TakePhotoFlags photoTaken(String subject, FImage processedPhoto) {
				return TakePhotoFlags.NONE;
			}
			
		};
	}
	
	void setPhotoAddedListener(PhotoAddedListener photoAddedListener) {
		this.photoAddedListener = photoAddedListener;
	}
	
	void setTakePhotoListener(TakePhotoListener<FImage> takePhotoListener) {
		this.takePhotoListener = takePhotoListener;
	}
	
	TakePhotoWindow() throws VideoCaptureException {
		
		super();
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		addWindowListener(this);
		setSize(300, 280);
		setTitle("Facerec");
		setLayout(new FlowLayout());
		try {
			setIconImage(Utils.getIcon());
		} catch (IOException e1) {
			System.out.println("Impossibile impostare l'icona");
			e1.printStackTrace();
		}
		setResizable(false);
		
		photoProcessor = new FlattenerCropperPhotoProcessor();
		
		this.name = "undefined";
		video = new MBFImageVideoSource();
		multiplePhotos = true;
		
		cmdTakePhoto = new JButton();
		cmdTakePhoto.setText("Scatta foto");
		cmdTakePhoto.setFont(font);
		cmdTakePhoto.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				MBFImage image = video.getFrame();
				FImage processed = photoProcessor.process(image);
				
				TakePhotoFlags flag = takePhotoListener.photoTaken(TakePhotoWindow.this.name, processed);
				if (flag == TakePhotoFlags.SUCCESS) {
					result.setText("Immagine salvata con successo");
					result.setForeground(new Color(0, 200, 0));
				} else {
					result.setText("Errore nel salvataggio dell'immagine");
					result.setForeground(Color.RED);
				}
				photoAddedListener.photoAdded();

				if (!multiplePhotos) {
					dispose();
				}
			}
			
		});
		
		result = new JLabel();
		result.setFont(font);
		
		add(((MBFImageVideoSource) video).getDisplay().getScreen());
		add(cmdTakePhoto);
		add(result);
	}
	
	/**
	 * Imposta il soggetto della foto.
	 * @param name
	 */
	public void setSubject(String name) {
		this.name = name;
	}
	
	/**
	 * Imposta la modalità per effettuare foto multiple o una foto singola.
	 * @param multiplePhotos true per poter scattare multiple foto, false se si vuole che la finestra venga chiusa dopo
	 * aver scattato la prima foto.
	 */
	public void setMultiplePhotos(boolean multiplePhotos) {
		this.multiplePhotos = multiplePhotos;
	}

	@Override
	public void windowOpened(WindowEvent e) {}

	@Override
	public void windowClosing(WindowEvent e) {}

	@Override
	public void windowClosed(WindowEvent e) {
		video.close();
	}

	@Override
	public void windowIconified(WindowEvent e) {}

	@Override
	public void windowDeiconified(WindowEvent e) {}

	@Override
	public void windowActivated(WindowEvent e) {}

	@Override
	public void windowDeactivated(WindowEvent e) {}
}
