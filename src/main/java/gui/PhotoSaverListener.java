package gui;

import java.io.File;
import java.io.IOException;

import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;

import utils.Files;
import utils.Settings;

/**
 * TakePhotoListener che salva la foto scattata.
 * @author Massimiliano Altieri
 */
public class PhotoSaverListener implements TakePhotoListener<FImage> {

	@Override
	public TakePhotoFlags photoTaken(String subject, FImage processedPhoto) {
		
		TakePhotoFlags flag = TakePhotoFlags.NONE;
		String photoName = Files.getAvailablePhotoName(subject);
		File file = new File(Settings.PHOTOS_PATH + subject + "\\" + photoName);
		
		try {
			ImageUtilities.write(processedPhoto, file);
			flag = TakePhotoFlags.SUCCESS;
		} catch (IOException f) {
			f.printStackTrace();
			flag = TakePhotoFlags.FAILURE;
		}
		
		return flag;
	}

}
