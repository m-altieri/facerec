package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker.StateValue;

import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.video.capture.VideoCaptureException;

import engine.ChunksListener;
import engine.EigenfacesEngine;
import exceptions.UncertainIdentityException;
import utils.Pair;
import utils.Settings;

/**
 * Schermata che fornisce il servizio di identificazione.
 * Il servizio di identificazione permette di inserire in input una foto rappresentante un volto, e di identificare
 * l'identità di quel soggetto, a patto che tale soggetto esista già tra gli utenti del sistema.
 * @author Massimiliano Altieri
 */
@SuppressWarnings("serial")
public class IdentificationPanel extends JPanel implements TakePhotoListener<FImage>, PropertyChangeListener, ChunksListener<Pair<String, Double>> {

	private JPanel leftPanel;
	private JPanel rightPanel;
	
	private FImage photo;
	private JPanel photoContainer;
	private JButton takePhoto;
	private JButton confirm;
	
	private ImageIcon loadingIcon;
	private JLabel loading;
	
	private JTextField identity;
	private JTextField confidence;
	
	private static final Font font = new Font("Arial", Font.PLAIN, 18);

	private EigenfacesEngine engine;
	
	IdentificationPanel() {
		
		super();
		
		setLayout(new GridLayout(1, 2));
		
		leftPanel = new JPanel();
		rightPanel = new JPanel();
		photoContainer = new JPanel();
		takePhoto = new JButton();
		confirm = new JButton();
		identity = new JTextField();
		confidence = new JTextField();
		engine = new EigenfacesEngine();
		
		loadingIcon = new ImageIcon(Settings.LOADING_ICON_PATH);		
		loading = new JLabel("Loading...", loadingIcon, JLabel.CENTER);
		loading.setFont(font);

		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		
		photoContainer.setPreferredSize(new Dimension(Settings.IMAGES_WIDTH, Settings.IMAGES_HEIGHT));
		
		takePhoto.setText("Acquisisci foto");
		takePhoto.setFont(font);
		takePhoto.setPreferredSize(new Dimension(200, 40));
		takePhoto.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					TakePhotoWindow window = new TakePhotoWindow();
					window.setVisible(true);
					window.setMultiplePhotos(false);
					window.setTakePhotoListener(IdentificationPanel.this);
				} catch (VideoCaptureException e1) {
					e1.printStackTrace();
				}
			}
			
		});
		
		leftPanel.add(takePhoto);		
		leftPanel.add(photoContainer);

		rightPanel.setLayout(new FlowLayout());

		confirm.setText("IDENTIFICA");
		confirm.setPreferredSize(new Dimension(250, 40));
		confirm.setFont(font);
		confirm.setEnabled(false);
		confirm.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				takePhoto.setEnabled(false);
				confirm.setEnabled(false);
				
				engine.setIdentificationPropertyChangeListener(IdentificationPanel.this);
				engine.setIdentificationChunksListener(IdentificationPanel.this);
				setLoading(true);
				
				engine.identify(photo);	
			}
			
		});

		rightPanel.add(confirm);
		
		JLabel lblIdentity = new JLabel("Identità");
		lblIdentity.setFont(font);
		identity.setPreferredSize(new Dimension(200, 40));
		identity.setFont(font);
		identity.setEnabled(false);
		JLabel lblAccuracy = new JLabel("Confidence");
		lblAccuracy.setFont(font);
		confidence.setPreferredSize(new Dimension(70, 40));
		confidence.setFont(font);
		confidence.setEnabled(false);
		
		rightPanel.add(lblIdentity);
		rightPanel.add(identity);
		rightPanel.add(lblAccuracy);
		rightPanel.add(confidence);
		
		add(leftPanel);
		add(rightPanel);
		
	}
	
	private void setLoading(boolean set) {
		if (set) {
			rightPanel.add(loading);
		} else {
			rightPanel.remove(loading);
		}
		paintAll(getGraphics());
	}

	@Override
	public gui.TakePhotoListener.TakePhotoFlags photoTaken(String subject, FImage processedPhoto) {
		photo = processedPhoto;
		photoContainer.removeAll();
		photoContainer.add(new JLabel(new ImageIcon(ImageUtilities.createBufferedImage(photo))));
		confirm.setEnabled(true);
		paintAll(getGraphics());
		return TakePhotoFlags.SUCCESS;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {

		if (evt.getNewValue() == StateValue.DONE) {
			Pair<String, Double> result;
			DecimalFormat decimalFormat = new DecimalFormat("##.##");
			try {
				result = engine.getIdentificationWorkerResult();
				if (result != null) {
					identity.setText(result.key());
					confidence.setText(decimalFormat.format(engine.distanceToSimilarity(result.value())));
				} else {
					identity.setText("Sconosciuta");
					confidence.setText("-");
				}
			} catch (UncertainIdentityException e) {
				System.out.println("Identità incerta");
				identity.setText(e.getName());
				confidence.setText(String.valueOf(decimalFormat.format(engine.distanceToSimilarity(e.getUncertainty()))));
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} finally {
				setLoading(false);
			}
		}
	}

	@Override
	public void chunksPublished(List<Pair<String, Double>> chunks) {
		System.out.println("Chunks received:");
		for (Pair<String, Double> chunk : chunks) {
			System.out.println("String: " + chunk.key());
			System.out.println("Double: " + chunk.value());
		}
	}

	
}
