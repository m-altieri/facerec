package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import utils.Files;

/**
 * Schermata per l'inserimento di nuovi utenti nel sistema.
 * @author Massimiliano Altieri
 */
@SuppressWarnings("serial")
public class AddUsersPanel extends JPanel {
	
	private JTextField txtName;
	private JButton cmdAdd;
	private JLabel result;
	
	private static final Font font = new Font("Arial", Font.PLAIN, 18);
	
	AddUsersPanel() {
		
		super();
		
		setLayout(new FlowLayout());
		
		txtName = new JTextField();
		txtName.setPreferredSize(new Dimension(300, 40));
		txtName.setFont(font);
				
		cmdAdd = new JButton();
		cmdAdd.setText("Aggiungi");
		cmdAdd.setPreferredSize(new Dimension(150, 40));
		cmdAdd.setFont(font);
		cmdAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String name = txtName.getText();
				if (!Files.existsUser(name)) {
					Files.createUser(name);
					result.setForeground(new Color(0, 200, 0));
					result.setText("Utente aggiunto!");
				} else {
					result.setForeground(Color.RED);
					result.setText(txtName.getText() + " esiste già.");
				}
			}
			
		});
		
		result = new JLabel();
		result.setFont(font);

		add(txtName);
		add(cmdAdd);
		add(result);
	}
	
}
