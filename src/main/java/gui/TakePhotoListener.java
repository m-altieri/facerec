package gui;

import org.openimaj.image.Image;

/**
 * Metodi di callback chiamati quando viene scattata una foto.
 * @author Massimiliano Altieri
 *
 * @param <T>
 */
public interface TakePhotoListener<T extends Image<?, ?>> {

	/**
	 * Metodo di callback chiamato quando viene scattata una foto.
	 * @param subject nome del soggetto in questione.
	 * @param processedPhoto foto scattata ed eventualmente elaborata da un PhotoProcessor.
	 * @return Un flag che può rappresentare buon fine o fallimento del metodo.
	 */
	TakePhotoFlags photoTaken(String subject, T processedPhoto);
	
	/**
	 * Flag che possono essere restituiti dal TakePhotoListener.
	 * @author Massimiliano Altieri
	 */
	enum TakePhotoFlags {
		SUCCESS,
		FAILURE,
		NONE,
	}
}
