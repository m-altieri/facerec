package utils;

/**
 * Classe di utilità che modella una coppia di valori.
 * @author Massimiliano Altieri
 *
 * @param <K> Tipo del primo elemento.
 * @param <V> Tipo del secondo elemento.
 */
public class Pair<K, V> {

	private K key;
	private V value;
	
	/**
	 * Crea una nuova coppia con i valori dati.
	 * @param key Il primo elemento.
	 * @param value Il secondo elemento.
	 */
	public Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Restituisce il primo elemento della coppia.
	 * @return Il primo elemento.
	 */
	public K key() {
		return key;
	}
	
	/**
	 * Restituisce il secondo elemento della coppia.
	 * @return Il secondo elemento.
	 */
	public V value() {
		return value;
	}
}
