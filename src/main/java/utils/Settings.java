package utils;

import java.io.File;
import java.io.IOException;

import org.ini4j.Ini;

/**
 * Classe di utilità contenente costanti utilizzate in tutto il sistema.
 * Le costanti vengono caricate dal file Settings.ini, che è diviso in due sezioni:
 * customization, che è modificabile e contiene vari parametri per perfezionare il sistema,
 * e statics, che non è modificabile e contiene valori come percorsi di file.
 * @author Massimiliano Altieri
 */
public final class Settings {

	/**
	 * Larghezza delle immagini dei volti utilizzate.
	 */
	public static int IMAGES_WIDTH;
	
	/**
	 * Altezza delle immagini dei volti utilizzate.
	 */
	public static int IMAGES_HEIGHT;
	
	/**
	 * Numero di eigenfaces utilizzati dall'algoritmo.
	 */
	public static int N_EIGENVECTORS;
	
	/**
	 * Numero di foto per ogni utente.
	 */
	public static int N_PHOTOS;
	
	/**
	 * Soglia di unknown, oltre la quale il riconoscimento fallisce.
	 */
	public static float UNKNOWN_THRESHOLD;
	
	/**
	 * Soglia di uncertain identity, oltre la quale il riconoscimento va a buon fine, ma non è garantito che
	 * sia esatto.
	 */
	public static float UNCERTAIN_THRESHOLD;
	
	/**
	 * Percorso delle foto degli utenti.
	 */
	public static String PHOTOS_PATH;
	
	/**
	 * Percorso dell'icona di caricamento.
	 */
	public static String LOADING_ICON_PATH;
	
	/**
	 * Larghezza della finestra della GUI.
	 */
	public static int WINDOW_WIDTH;
	
	/**
	 * Altezza della finestra della GUI.
	 */
	public static int WINDOW_HEIGHT;
	
	private static final String customization_sectionname = "customization";
	private static final String statics_sectionname = "statics";
	
	private static final String images_width_keyname = "images_width";
	private static final String images_height_keyname = "images_height";
	private static final String n_eigenvectors_keyname = "n_eigenvectors";
	private static final String n_photos_keyname = "n_photos";
	private static final String unknown_threshold_keyname= "unknown_threshold";
	private static final String uncertain_threshold_keyname = "uncertain_threshold";
	
	private static final String photos_path_keyname = "photos_path";
	private static final String loading_path_keyname = "loading_icon_path";
	private static final String window_width_keyname = "window_width";
	private static final String window_height_keyname = "window_height";
	
	static {
		
		try {
			File settingsFile = new File("settings.ini");
			Ini ini = new Ini(settingsFile);
			
			String imagesWidthValue = ini.get(customization_sectionname, images_width_keyname);
			String imagesHeightValue = ini.get(customization_sectionname, images_height_keyname);
			String nEigenvectorsValue = ini.get(customization_sectionname, n_eigenvectors_keyname);
			String nPhotos = ini.get(customization_sectionname, n_photos_keyname);
			String unknownThresholdValue = ini.get(customization_sectionname, unknown_threshold_keyname);
			String uncertainThresholdValue = ini.get(customization_sectionname, uncertain_threshold_keyname);
			
			String photosPathValue = ini.get(statics_sectionname, photos_path_keyname);
			String loadingIconPathValue = ini.get(statics_sectionname, loading_path_keyname);
			String windowWidthValue = ini.get(statics_sectionname, window_width_keyname);
			String windowHeightValue = ini.get(statics_sectionname, window_height_keyname);
			
			IMAGES_WIDTH = Integer.parseInt(imagesWidthValue);
			IMAGES_HEIGHT = Integer.parseInt(imagesHeightValue);
			N_EIGENVECTORS = Integer.parseInt(nEigenvectorsValue);
			N_PHOTOS = Integer.parseInt(nPhotos);
			UNKNOWN_THRESHOLD = Float.parseFloat(unknownThresholdValue);
			UNCERTAIN_THRESHOLD = Float.parseFloat(uncertainThresholdValue);
			
			PHOTOS_PATH = photosPathValue;
			LOADING_ICON_PATH = loadingIconPathValue;
			WINDOW_WIDTH = Integer.parseInt(windowWidthValue);
			WINDOW_HEIGHT = Integer.parseInt(windowHeightValue);
						
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private Settings() {
		// Cannot be instantiated.
	}
}
