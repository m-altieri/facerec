package utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Classe di utilità non istanziabile che fornisce una serie di metodi statici utili nel sistema, 
 * in particolare per la gestione dei file.
 * @author Massimiliano Altieri
 */
public final class Files {
	
	private Files() {
		// Cannot be instantiated.
	}
	
	/**
	 * Restituisce il numero di foto di un certo utente.
	 * @param name Il nome dell'utente.
	 * @return Il numero di foto.
	 */
	public static int numberOfPhotos(String name) {
		
		File directory = new File(Settings.PHOTOS_PATH + name + "\\");
		File[] photos = directory.listFiles();
		
		return photos.length;
	}
	
	/**
	 * Restituisce un nome per una foto non utilizzato dallo stesso utente.
	 * Normalmente dovrebbe seguire una numerazione crescenta <i>(es. "1.jpg", "2.jpg", ...)</i>
	 * @param name Il nome dell'utente.
	 * @return Il nome disponibile per la foto.
	 */
	public static String getAvailablePhotoName(String name) {
		
		File directory = new File(Settings.PHOTOS_PATH + name + "\\");
		File[] photos = directory.listFiles();
		
		List<File> photoFiles = new ArrayList<File>(Arrays.asList(photos));
		photoFiles.sort(new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				
				String o1Name = o1.getName();
				String o2Name = o2.getName();
				
				String[] o1NameStructure = o1Name.split("\\.");
				String[] o2NameStructure = o2Name.split("\\.");
				
				int o1NameNumber = Integer.parseInt(o1NameStructure[0]);
				int o2NameNumber = Integer.parseInt(o2NameStructure[0]);
				
				if (o1NameNumber < o2NameNumber) {
					return -1;
				} else {
					return 1;
				}
			}
			
		});
		
		String availableName = null;
		
		if (photoFiles.size() > 0) {
			File lastPhoto = photoFiles.get(photoFiles.size() - 1);
			
			String[] lastPhotoNameStructure = lastPhoto.getName().split("\\.");
			
			int availableNumber = Integer.parseInt(lastPhotoNameStructure[0]) + 1;
			
			availableName = Integer.toString(availableNumber) + ".jpg";
		} else {
			availableName = "1.jpg";
		}
		
		return availableName;
	}
	
	/**
	 * Restituisce una lista contenente tutti gli utenti del sistema.
	 * @return Lista contenente gli utenti del sistema.
	 */
	public static List<String> getUsers() {
		
		List<String> users = null;
		
		File directory = new File(Settings.PHOTOS_PATH);

		String[] folders = directory.list();
		users = Arrays.asList(folders);

		return users;
	}
	
	/**
	 * Restituisce una lista contenente tutti gli utenti del sistema aventi il numero minimo di foto.
	 * @return Lista degli utenti completi.
	 */
	public static List<String> getCompleteUsers() {
		
		List<String> users = getUsers();
		List<String> usersWithAllPhotos = new ArrayList<String>();
		for (String user : users) {
			int nPhotos = numberOfPhotos(user);
			if (nPhotos >= Settings.N_PHOTOS) {
				usersWithAllPhotos.add(user);
			}
		}
		String[] validUsers = new String[usersWithAllPhotos.size()];
		for (int i = 0; i < validUsers.length; i++) {
			validUsers[i] = usersWithAllPhotos.get(i);
		}
		
		return Arrays.asList(validUsers);
	}
	
	/**
	 * Restituisce una lista contenente tutti i nomi delle foto di un certo utente.
	 * @param user Il nome dell'utente.
	 * @return La lista contenente i nomi delle foto.
	 */
	public static List<String> getPhotos(String user) {
		
		List<String> photos = null;
		
		File directory = new File(Settings.PHOTOS_PATH + user + "\\");
		String[] photosFiles = directory.list();
		
		photos = Arrays.asList(photosFiles);
		return photos;
	}
	
	/**
	 * Controlla l'esistenza di un utente.
	 * @param name Il nome dell'utente.
	 * @return true se l'utente è presente nel sistema, false altrimenti.
	 */
	public static boolean existsUser(String name) {
		File folder = new File(Settings.PHOTOS_PATH + name);
		return folder.exists();
	}
	
	/**
	 * Crea un nuovo utente con il dato nome.
	 * @param name Il nome dell'utente.
	 */
	public static void createUser(String name) {
		File folder = new File(Settings.PHOTOS_PATH + name);
		folder.mkdirs();
	}
}
